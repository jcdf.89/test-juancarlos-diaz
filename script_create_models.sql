-- -----------------------------------------------------
-- Creación del esquema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS 'employees' DEFAULT CHARACTER SET latin1 ;
USE 'employees' ;

-- -----------------------------------------------------
-- Creación de la Tabla 'GENDERS'
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS 'employees'.'GENDERS' (
  'ID' INT(11) NOT NULL,
  'NAME' VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY ('ID'));



-- -----------------------------------------------------
-- Creación de la Tabla 'JOBS'
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS 'employees'.'JOBS' (
  'ID' INT(11) NOT NULL,
  'NAME' VARCHAR(255) NULL DEFAULT NULL,
  'SALARY' DECIMAL(9,2) NULL DEFAULT NULL,
  PRIMARY KEY ('ID'));


-- -----------------------------------------------------
-- Creación de la Tabla 'EMPLOYEES'
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS 'employees'.'EMPLOYEES' (
  'ID' INT(11) NOT NULL,
  'GENDER_ID' INT(11) NOT NULL,
  'JOB_ID' INT(11) NULL DEFAULT NULL,
  'NAME' VARCHAR(255) NULL DEFAULT NULL,
  'LAST_NAME' VARCHAR(255) NULL DEFAULT NULL,
  'BIRTHDATE' VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY ('ID'),
  INDEX 'fk_employees_gender_id_idx' ('GENDER_ID' ASC) VISIBLE,
  INDEX 'fk_employees_job_id_idx' ('JOB_ID' ASC) VISIBLE,
  CONSTRAINT 'fk_employees_gender_id'
    FOREIGN KEY ('GENDER_ID')
    REFERENCES 'employees'.'GENDERS' ('ID')
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT 'fk_employees_job_id'
    FOREIGN KEY ('JOB_ID')
    REFERENCES 'employees'.'JOBS' ('ID')
);


-- -----------------------------------------------------
-- Creación de la Tabla 'EMPLOYEE_WORKED_HOURS'
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS 'employees'.'EMPLOYEE_WORKED_HOURS' (
  'ID' INT(11) NOT NULL,
  'EMPLOYEE_ID' INT(11) NULL DEFAULT NULL,
  'WORKED_HOURS' INT(11) NULL DEFAULT NULL,
  'WORKED_DATE' DATE NULL DEFAULT NULL,
  PRIMARY KEY ('ID'),
  INDEX 'fk_worked_hours_employee_id_idx' ('EMPLOYEE_ID' ASC) VISIBLE,
  CONSTRAINT 'fk_worked_hours_employee_id'
    FOREIGN KEY ('EMPLOYEE_ID')
    REFERENCES 'employees'.'EMPLOYEES' ('ID')
);