# test-juancarlos-diaz

Project test Grupo Nach Java

## Descripción
Proyecto para el test de grupo nachpara la vacante de desarrollador backend


**Query**
En el archivo script_create_models.sql se encuentran los querys para la creación de la base de datos.


##Apis Desarrolladas

**Ejercicio 1:**

    **URL**: http://localhost:8080/api/employees
    **Tipo de request**: POST
    **Parametros de entrada**:

        {
            "gender_id": 1,
            "job_id": 2, 
            "name": "José", 
            "last_name": "López", 
            "birthdate": "1980-12-03" 
        }

    **Parametros de salida**:
        {
           "success": true,
            "id": 2 
        }

**Ejercicio 2:**

    **URL**: http://localhost:8080/api/employees/workedhours/
    **Tipo de request**: POST
    **Parametros de entrada**:

        {
            "employee_id": 2, 
            "worked_hours": 10,
            "worked_date": "2022-06-1"
        }

    **Parametros de salida**:
        {
            "success": true,
            "id": 6
        }   


**Ejercicio 3:**

    **URL**: http://localhost:8080/api/employees/search/
    **Tipo de request**: POST
    **Parametros de entrada**:

        {
            "job_id": 4
        }

    **Parametros de salida**:
        **Cuando es satisfactorio**

        {
            "success": true,
            "employees": [
                {
                    "id": 1,
                    "gender": {
                        "id": 1,
                        "name": "masculino"
                    },
                    "job": {
                        "id": 1,
                        "name": "developer junior",
                        "salary": 200
                    },
                    "name": "Juan",
                    "lastName": "Pérez",
                    "birthdate": "1983-01-01"
                }
            ]
        }

        **Cuando sucede un error:**
        {
            "success": false,
            "error:": "No existe el puesto proporcionado"
        }  

**Ejercicio 4:**

    **URL**: http://localhost:8080/api/employees/total-worked-hours/
    **Tipo de request**: POST
    **Parametros de entrada**:

        {
            "employee_id": 1,
            "start_date": "2022-07-25", 
            "end_date": "2022-07-20"
        }

    **Parametros de salida**:
        **Cuando es satisfactorio**

       {
            "total_worked_hours": 45,
            "success": true
        }
        
        **Cuando sucede un error:**
        {
            "success": false,
            "error:": "La fecha de inicio debe ser menor que la fecha de fin"
        }  

**Ejercicio 5:**

    **URL**: http://localhost:8080/api/employees/total-payment/
    **Tipo de request**: POST
    **Parametros de entrada**:

        {
            "employee_id": 1,
            "start_date": "2022-07-25", 
            "end_date": "2022-07-20"
        }

    **Parametros de salida**:
        **Cuando es satisfactorio**

       {
            "success": true,
            "payment": 5000
        }
        
        **Cuando sucede un error:**
        {
            "success": false,
            "error:": "La fecha de inicio debe ser menor que la fecha de fin"
        }  