package com.test.juancarlosdiaz.nach.testapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.juancarlosdiaz.nach.testapi.models.Genders;
import com.test.juancarlosdiaz.nach.testapi.repository.GenderRepository;

@Service
public class GendersService {

    @Autowired
    private GenderRepository genderRepository;

    public List<Genders> list() {
        return genderRepository.findAll();
    }

    public Optional<Genders> getById(Long id) {
        return genderRepository.findById(id);
    }
}
