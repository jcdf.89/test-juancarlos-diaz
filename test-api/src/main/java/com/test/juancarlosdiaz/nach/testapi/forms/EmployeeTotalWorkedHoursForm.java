package com.test.juancarlosdiaz.nach.testapi.forms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeTotalWorkedHoursForm {
    @JsonProperty("employee_id")
    private Long employeeId;

    @JsonProperty("start_date")
    private String startDate;

    @JsonProperty("end_date")
    private String endDate;

    public EmployeeTotalWorkedHoursForm() {

    }

    public EmployeeTotalWorkedHoursForm(Long employeeId, String startDate, String endDate) {
        this.employeeId = employeeId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
