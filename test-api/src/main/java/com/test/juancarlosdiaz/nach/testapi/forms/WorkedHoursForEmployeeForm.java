package com.test.juancarlosdiaz.nach.testapi.forms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WorkedHoursForEmployeeForm {
    @JsonProperty("employee_id")
    private Long employeeId;

    @JsonProperty("worked_hours")
    private Integer workedHours;

    @JsonProperty("worked_date")
    private String workedDate;

    public WorkedHoursForEmployeeForm() {

    }

    public WorkedHoursForEmployeeForm(Long employeeId, Integer workedHours, String workedDate) {
        this.employeeId = employeeId;
        this.workedHours = workedHours;
        this.workedDate = workedDate;

    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(Integer workedHours) {
        this.workedHours = workedHours;
    }

    public String getWorkedDate() {
        return workedDate;
    }

    public void setWorkedDate(String workedDate) {
        this.workedDate = workedDate;
    }
}
