package com.test.juancarlosdiaz.nach.testapi.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.test.juancarlosdiaz.nach.testapi.models.Jobs;

public interface JobsRepository extends JpaRepository<Jobs, Long> {
    List<Jobs> findByNameContaining(String name);

    List<Jobs> findBySalaryContaining(String salary);
}