package com.test.juancarlosdiaz.nach.testapi.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.juancarlosdiaz.nach.testapi.models.Employee;
import com.test.juancarlosdiaz.nach.testapi.models.EmployeeWorkedHours;
import com.test.juancarlosdiaz.nach.testapi.repository.EmployeeWorkedHoursRepository;

@Service
public class WorkedHoursEmployeeService {

    @Autowired
    private EmployeeWorkedHoursRepository employeeWorkedHoursRepository;

    public List<EmployeeWorkedHours> list() {
        return employeeWorkedHoursRepository.findAll();
    }

    public Optional<EmployeeWorkedHours> getById(Long id) {
        return employeeWorkedHoursRepository.findById(id);
    }

    public List<EmployeeWorkedHours> listByEmployeeAndWorkedDate(Employee employee, Date workedDate) {
        return employeeWorkedHoursRepository.findByEmployeeAndWorkedDate(employee, workedDate);
    }

    public EmployeeWorkedHours save(EmployeeWorkedHours employee) {
        return employeeWorkedHoursRepository.save(employee);
    }

    public Integer getTotalWorkedHoursByEmployeeAndWorkedDateBetween(Employee employee, Date dateTo,
            Date dateFrom) {
        Integer totalWorkedHours = 0;
        List<EmployeeWorkedHours> listWorkedHoursEmployee = employeeWorkedHoursRepository
                .findByEmployeeAndWorkedDateBetween(employee, dateTo, dateFrom);

        totalWorkedHours = listWorkedHoursEmployee.stream().mapToInt(w -> w.getWorkedHours()).sum();

        return totalWorkedHours;
    }

    public Float getTotalPaymentByEmployeeAndWorkedDateBetween(Employee employee, Date dateTo,
            Date dateFrom) {
        Integer totalWorkedHours = 0;
        List<EmployeeWorkedHours> listWorkedHoursEmployee = employeeWorkedHoursRepository
                .findByEmployeeAndWorkedDateBetween(employee, dateTo, dateFrom);

        totalWorkedHours = listWorkedHoursEmployee.stream().mapToInt(w -> w.getWorkedHours()).sum();

        return totalWorkedHours * employee.getJob().getSalary();
    }

    public boolean IsLessThanOrEqualCurrentDate(String workedDate) throws ParseException {
        boolean isLessThanOrEqualCurrentDate = false;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(workedDate);

        Calendar calendar = Calendar.getInstance();
        Calendar currentDate = Calendar.getInstance();
        calendar.setTime(date);

        if (calendar.compareTo(currentDate) >= 0) {
            isLessThanOrEqualCurrentDate = true;
        }

        return isLessThanOrEqualCurrentDate;
    }

    public Date convertStringToDate(String stringDate) throws ParseException {
        Date date = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        date = formatDate.parse(stringDate);
        return date;
    }

    public boolean validateStartDateAndEndDate(Date startDate, Date endDate) throws ParseException {
        boolean isValid = false;

        Calendar _startDate = Calendar.getInstance();
        Calendar _endDate = Calendar.getInstance();
        _startDate.setTime(startDate);
        _endDate.setTime(endDate);

        if (_endDate.compareTo(_startDate) >= 0) {
            isValid = true;
        }

        return isValid;
    }

}
