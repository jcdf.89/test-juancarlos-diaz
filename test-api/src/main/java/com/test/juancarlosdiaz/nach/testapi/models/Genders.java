package com.test.juancarlosdiaz.nach.testapi.models;

import javax.persistence.*;;

@Entity
@Table(name = "Genders")
public class Genders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;

    public Genders() {

    }

    public Genders(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
