package com.test.juancarlosdiaz.nach.testapi.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.juancarlosdiaz.nach.testapi.models.Employee;
import com.test.juancarlosdiaz.nach.testapi.models.Jobs;
import com.test.juancarlosdiaz.nach.testapi.repository.EmployeeRepository;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<Employee> list() {
        return employeeRepository.findAll();
    }

    public Optional<Employee> getById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> listByName(String name) {
        return employeeRepository.findByNameContaining(name);
    }

    public List<Employee> listByLastName(String lastName) {
        return employeeRepository.findByLastNameContaining(lastName);
    }

    public List<Employee> listByJob(Jobs job) {
        return employeeRepository.findByJob(job);
    }

    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    public boolean IsLegalAge(String birthdate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(birthdate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        boolean isLegal = false;
        int yearBirthdate = calendar.get(Calendar.YEAR);
        int monthBirthdate = calendar.get(Calendar.MONTH);
        int dayBirthdate = calendar.get(Calendar.DAY_OF_MONTH);

        Calendar birthDay = new GregorianCalendar(yearBirthdate, monthBirthdate, dayBirthdate);
        Calendar today = new GregorianCalendar();

        today.setTime(new Date());
        int yearsInBetween = today.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        if (yearsInBetween > 18) {
            isLegal = true;
        }

        return isLegal;
    }
}
