package com.test.juancarlosdiaz.nach.testapi.models;

import javax.persistence.*;;

@Entity
@Table(name = "JOBS")
public class Jobs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SALARY")
    private Float salary;

    public Jobs() {

    }

    public Jobs(String name, Float salary) {
        this.name = name;
        this.salary = salary;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

}
