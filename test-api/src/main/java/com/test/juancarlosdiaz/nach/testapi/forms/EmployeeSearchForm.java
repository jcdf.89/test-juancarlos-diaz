package com.test.juancarlosdiaz.nach.testapi.forms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeSearchForm {
    @JsonProperty("job_id")
    private Long jobId;

    public EmployeeSearchForm() {

    }

    public EmployeeSearchForm(Long job_id) {
        this.jobId = job_id;
    }

    public Long getJobId() {
        return jobId;
    }
}
