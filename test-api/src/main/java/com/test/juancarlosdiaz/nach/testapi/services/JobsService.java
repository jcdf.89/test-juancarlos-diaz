package com.test.juancarlosdiaz.nach.testapi.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.juancarlosdiaz.nach.testapi.models.Jobs;
import com.test.juancarlosdiaz.nach.testapi.repository.JobsRepository;

@Service
public class JobsService {

    @Autowired
    private JobsRepository jobsRepository;

    public List<Jobs> list() {
        return jobsRepository.findAll();
    }

    public Optional<Jobs> getById(Long id) {
        return jobsRepository.findById(id);
    }
}
