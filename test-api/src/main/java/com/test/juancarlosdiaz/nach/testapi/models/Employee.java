package com.test.juancarlosdiaz.nach.testapi.models;

import javax.persistence.*;

@Entity
@Table(name = "EMPLOYEES")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "GENDER_ID")
    private Genders gender;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "JOB_ID")
    private Jobs job;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "BIRTHDATE")
    private String birthdate;

    public Employee() {

    }

    public Employee(Genders gender, Jobs job, String name, String last_name, String birth_date) {
        this.gender = gender;
        this.job = job;
        this.name = name;
        this.lastName = last_name;
        this.birthdate = birth_date;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Genders getGender() {
        return gender;
    }

    public Jobs getJob() {
        return job;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

}
