package com.test.juancarlosdiaz.nach.testapi.models;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "EMPLOYEE_WORKED_HOURS")
public class EmployeeWorkedHours {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "EMPLOYEE_ID")
    private Employee employee;

    @Column(name = "WORKED_HOURS")
    private Integer workedHours;

    @Column(name = "WORKED_DATE")
    private Date workedDate;

    public EmployeeWorkedHours() {

    }

    public EmployeeWorkedHours(Employee employee, Integer workedHours, Date workedDate) {
        this.employee = employee;
        this.workedHours = workedHours;
        this.workedDate = workedDate;
    }

    public long getId() {
        return id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Integer getWorkedHours() {
        return workedHours;
    }

    public void setWorkedHours(Integer workedHours) {
        this.workedHours = workedHours;
    }

    public Date getWorkedDate() {
        return workedDate;
    }

    public void setWorkedDate(Date workedDate) {
        this.workedDate = workedDate;
    }

}
