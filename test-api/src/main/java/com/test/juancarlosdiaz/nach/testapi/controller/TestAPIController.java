package com.test.juancarlosdiaz.nach.testapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.juancarlosdiaz.nach.testapi.forms.*;
import com.test.juancarlosdiaz.nach.testapi.models.*;
import com.test.juancarlosdiaz.nach.testapi.services.*;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/api")
public class TestAPIController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    GendersService genderService;

    @Autowired
    JobsService jobsService;

    @Autowired
    WorkedHoursEmployeeService workedHoursEmployeeService;

    @RequestMapping("/")
    String hellow() {
        return "Hello World JC!";
    }

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllTutorials(@RequestParam(required = false) String name) {
        try {
            List<Employee> employees = new ArrayList<Employee>();

            if (name == null)
                employeeService.list().forEach(employees::add);
            else
                employeeService.listByLastName(name).forEach(employees::add);

            if (employees.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(employees, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employees")
    public ResponseEntity<Object> createEmployee(@RequestBody EmployeeForm employeeForm) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            List<Employee> employees_by_name = employeeService.listByName(employeeForm.getName());
            if (employees_by_name.size() > 0) {
                map.put("error:", "Ya existe el nombre proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);

            }
            List<Employee> employees_by_last_name = employeeService
                    .listByLastName(employeeForm.getLastName());
            if (employees_by_last_name.size() > 0) {
                map.put("error:", "Ya existe el apellido proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);

            }
            Optional<Genders> _gender = genderService.getById(employeeForm.getGenderId());

            if (!_gender.isPresent()) {
                map.put("error:", "No existe el género proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            if (!employeeService.IsLegalAge(employeeForm.getBirthdate())) {
                map.put("error:", "El empleado no es mayor de edad");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            Optional<Jobs> _jobs = jobsService.getById(employeeForm.getJobId());
            if (!_jobs.isPresent()) {
                map.put("error:", "No existe el puesto proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            Employee _employee = employeeService
                    .save(new Employee(_gender.get(), _jobs.get(), employeeForm.getName(),
                            employeeForm.getLastName(),
                            employeeForm.getBirthdate()));

            map.put("id", _employee.getId());
            map.put("success", true);

            return new ResponseEntity<Object>(map, HttpStatus.CREATED);
        } catch (Exception e) {
            map.put("id", null);
            map.put("success", false);
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employees/workedhours")
    public ResponseEntity<Object> createWorkedHoursForEmployee(
            @RequestBody WorkedHoursForEmployeeForm workedHoursForEmployeeForm) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            Date workedDate = workedHoursEmployeeService
                    .convertStringToDate(workedHoursForEmployeeForm.getWorkedDate());

            Optional<Employee> _employee = employeeService.getById(workedHoursForEmployeeForm.getEmployeeId());
            if (!_employee.isPresent()) {
                map.put("error:", "No existe el empleado proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            if (workedHoursForEmployeeForm.getWorkedHours() > 20) {
                map.put("error:", "El total de horas no puede ser mayor a 20");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            if (workedHoursEmployeeService.IsLessThanOrEqualCurrentDate(workedHoursForEmployeeForm.getWorkedDate())) {
                map.put("error:", "La fecha de trabajo debe ser menor o igual a la fecha actual");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            if (workedHoursEmployeeService.listByEmployeeAndWorkedDate(_employee.get(), workedDate).size() > 0) {
                map.put("error:", "Ya existe un registro para este empleado con la fecha proporcionada");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            EmployeeWorkedHours workedHourForEmployee = workedHoursEmployeeService
                    .save(new EmployeeWorkedHours(_employee.get(), workedHoursForEmployeeForm.getWorkedHours(),
                            workedDate));

            map.put("id", workedHourForEmployee.getId());
            map.put("success", true);
            return new ResponseEntity<Object>(map, HttpStatus.CREATED);
        } catch (Exception e) {
            map.put("id", null);
            map.put("success", false);
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employees/search")
    public ResponseEntity<Object> searchEmployees(
            @RequestBody EmployeeSearchForm employeeSearchForm) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            Optional<Jobs> _job = jobsService.getById(employeeSearchForm.getJobId());
            if (!_job.isPresent()) {
                map.put("error:", "No existe el puesto proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            List<Employee> employees = employeeService.listByJob(_job.get());

            map.put("employees", employees);
            map.put("success", true);
            return new ResponseEntity<Object>(map, HttpStatus.CREATED);
        } catch (Exception e) {
            map.put("employees", null);
            map.put("success", false);
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employees/total-worked-hours")
    public ResponseEntity<Object> totalWorkedHoursBetweenDatesByEmployee(
            @RequestBody EmployeeTotalWorkedHoursForm employeeTotalWorkedHoursForm) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            Date startDate = workedHoursEmployeeService
                    .convertStringToDate(employeeTotalWorkedHoursForm.getStartDate());
            Date endDate = workedHoursEmployeeService
                    .convertStringToDate(employeeTotalWorkedHoursForm.getEndDate());

            Optional<Employee> _employee = employeeService.getById(employeeTotalWorkedHoursForm.getEmployeeId());
            if (!_employee.isPresent()) {
                map.put("error:", "No existe el empleado proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            if (!workedHoursEmployeeService.validateStartDateAndEndDate(startDate, endDate)) {
                map.put("error:", "La fecha de inicio debe ser menor que la fecha de fin");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            Integer totalWorkedHours = workedHoursEmployeeService
                    .getTotalWorkedHoursByEmployeeAndWorkedDateBetween(_employee.get(), startDate, endDate);

            map.put("total_worked_hours", totalWorkedHours);
            map.put("success", true);
            return new ResponseEntity<Object>(map, HttpStatus.CREATED);
        } catch (Exception e) {
            map.put("total_worked_hours", null);
            map.put("success", false);
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employees/total-payment")
    public ResponseEntity<Object> totalEmployePaymentBetweenDates(
            @RequestBody EmployeeTotalWorkedHoursForm employeeTotalWorkedHoursForm) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            Date startDate = workedHoursEmployeeService
                    .convertStringToDate(employeeTotalWorkedHoursForm.getStartDate());
            Date endDate = workedHoursEmployeeService
                    .convertStringToDate(employeeTotalWorkedHoursForm.getEndDate());

            Optional<Employee> _employee = employeeService.getById(employeeTotalWorkedHoursForm.getEmployeeId());
            if (!_employee.isPresent()) {
                map.put("error:", "No existe el empleado proporcionado");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            if (!workedHoursEmployeeService.validateStartDateAndEndDate(startDate, endDate)) {
                map.put("error:", "La fecha de inicio debe ser menor que la fecha de fin");
                map.put("success", false);
                return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
            }

            Float totalPayment = workedHoursEmployeeService
                    .getTotalPaymentByEmployeeAndWorkedDateBetween(_employee.get(), startDate, endDate);

            map.put("payment", totalPayment);
            map.put("success", true);
            return new ResponseEntity<Object>(map, HttpStatus.CREATED);
        } catch (Exception e) {
            map.put("payment", null);
            map.put("success", false);
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
