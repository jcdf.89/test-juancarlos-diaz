package com.test.juancarlosdiaz.nach.testapi.forms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EmployeeForm {
    @JsonProperty("gender_id")
    private Long genderId;

    @JsonProperty("job_id")
    private Long jobId;

    private String name;

    @JsonProperty("last_name")
    private String lastName;

    private String birthdate;

    public EmployeeForm() {

    }

    public EmployeeForm(Long gender_id, Long job_id, String name, String last_name, String birth_date) {
        this.genderId = gender_id;
        this.jobId = job_id;
        this.name = name;
        this.lastName = last_name;
        this.birthdate = birth_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGenderId() {
        return genderId;
    }

    public Long getJobId() {
        return jobId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String last_name) {
        this.lastName = last_name;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
}
