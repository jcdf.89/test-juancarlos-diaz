package com.test.juancarlosdiaz.nach.testapi.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.test.juancarlosdiaz.nach.testapi.models.Employee;
import com.test.juancarlosdiaz.nach.testapi.models.EmployeeWorkedHours;

public interface EmployeeWorkedHoursRepository extends JpaRepository<EmployeeWorkedHours, Long> {

    List<EmployeeWorkedHours> findByEmployeeAndWorkedDate(Employee employee, Date workedDate);

    List<EmployeeWorkedHours> findByEmployeeAndWorkedDateBetween(Employee employee, Date to, Date from);
}
