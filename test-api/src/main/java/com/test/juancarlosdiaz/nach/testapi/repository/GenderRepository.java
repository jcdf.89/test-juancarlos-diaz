package com.test.juancarlosdiaz.nach.testapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.test.juancarlosdiaz.nach.testapi.models.Genders;

public interface GenderRepository extends JpaRepository<Genders, Long> {
}