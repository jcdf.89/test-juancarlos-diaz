package com.test.juancarlosdiaz.nach.testapi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.test.juancarlosdiaz.nach.testapi.models.*;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    List<Employee> findByNameContaining(String name);

    List<Employee> findByLastNameContaining(String lastName);

    List<Employee> findByJob(Jobs job);
}